
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.aksw.owl2sparql.OWLAxiomToSPARQLConverter;
import org.aksw.owl2sparql.OWLClassExpressionToSPARQLConverter;
import org.aksw.owl2sparql.style.AllQuantorTranslation;
//import org.apache.jena.query.Query;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.ManchesterSyntaxDocumentFormat;
import org.semanticweb.owlapi.io.OWLParser;
import org.semanticweb.owlapi.io.ToStringRenderer;
import org.semanticweb.owlapi.manchestersyntax.parser.ManchesterOWLSyntaxOntologyParser;
import org.semanticweb.owlapi.manchestersyntax.parser.ManchesterOWLSyntaxOntologyParserFactory;
import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxOWLObjectRendererImpl;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.ClassExpressionType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom;
import org.semanticweb.owlapi.model.OWLAnnotationSubject;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyLoaderConfiguration;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.rdf.turtle.parser.TurtleOntologyParser;
import org.semanticweb.owlapi.util.OWLClassExpressionCollector;

import com.hp.hpl.jena.query.Query;


public class MyTest {
	public static void main(String[] args) throws Exception
	{
		OWLParser  parser =null;
		TurtleOntologyParser tParser =null;
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		parser = new ManchesterOWLSyntaxOntologyParserFactory().createParser();

		OWLDataFactory df = manager.getOWLDataFactory();
		//IRI iri = IRI.create(new File("/Users/joyce/git/kyc-product/system_models/ontology_model/legalentityManSyntax.txt"));
		IRI iri = IRI.create(new File("/Users/joyce/git/kyc-product/system_models/ontology_model/legalentity2.ttl"));
		BufferedOutputStream outputStream=new BufferedOutputStream(new FileOutputStream("/Users/joyce/git/kyc-product/system_models/ontology_model/legalentityOut.txt"));
	      
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(iri);
        OWLDocumentFormat manFormat =new ManchesterSyntaxDocumentFormat();
		
		manager.saveOntology(ontology, manFormat, outputStream);
		
		manager.removeOntology(ontology);
		
//		OWLOntologyLoaderConfiguration config = new OWLOntologyLoaderConfiguration();
//		 config.setMissingOntologyHeaderStrategy( OWLOntologyLoaderConfiguration.MissingOntologyHeaderStrategy.IMPORT_GRAPH );

		 ontology = manager.loadOntologyFromOntologyDocument(IRI.create(new File("/Users/joyce/git/kyc-product/system_models/ontology_model/legalentityOut.txt")));
		 
		//OWLClass owlClass= manager.getOWLDataFactory().getOWLClass(IRI.create("http://nextangles.com/common/1.0/legalentity#PubliclyTradedLegalEntity"));

		    OWLClassExpressionCollector col= new OWLClassExpressionCollector();
		    Collection<OWLClassExpression> clss = col.visit(ontology);
		    
//	        OWLClassExpressionToSPARQLConverter classExpconverter = new OWLClassExpressionToSPARQLConverter();
//			classExpconverter.setAllQuantorTranslation(AllQuantorTranslation.DOUBLE_NEGATION);
//		
//			   Iterator<OWLClassExpression> classExpressionsIt = clss.iterator();
//				    while (classExpressionsIt.hasNext()) {
//				         OWLClassExpression classExpression = (OWLClassExpression)classExpressionsIt.next();
////				         if(classExpression.toString().contains("true\"^^xsd:boolean"))
////				         {
////				//        	 classExpression.accept( myVisitor());
////				        	 //.getClassExpressionType( ClassExpressionType.DATA_HAS_VALUE);
////				         }
////				
//				         Set<OWLDataProperty> dts =classExpression.getDataPropertiesInSignature();
//				         com.hp.hpl.jena.query.Query query = classExpconverter.asQuery(classExpression,"?this");
//				         System.out.println("\r\nfrom class expression: \r\n"+ query);
//				    }
////					   
	      
	for (OWLClass cls : ontology.getClassesInSignature())
	{
		System.out.println(cls);
		Set<OWLClassAxiom> axioms = ontology.getAxioms(cls);
		//Set<OWLAxiom> axioms = ontology.getAxioms();
		//Set<OWLLogicalAxiom> axioms = ontology.getLogicalAxioms();
			Query query = null;
			for(OWLClassAxiom axiom:axioms)
			{
				Set <OWLEntity> oes = axiom.getSignature();
				OWLAxiomToSPARQLConverter c = new OWLAxiomToSPARQLConverter();
				if(axiom.isAnnotationAxiom() ) continue;
				if(axiom.isOfType(AxiomType.ABoxAxiomTypes) | axiom.isOfType(AxiomType.DECLARATION) ) continue;
				
				try{ 
					System.out.println("********"+axiom.getAxiomType() + ":"+axiom);	
					
					query = c.asQuery(axiom, cls);
				System.out.println("converted:\r\n"+ query.toString());
				}catch(Exception e){ e.printStackTrace();}
			}
	}  
			
			
	//		printAxioms(axioms);
		//	Set<OWLClass> classes = ontology.getClassesInSignature();
		//	printClasses(classes);
			//printLogicalAxioms(ontology.getLogicalAxioms());

//			Set<OWLEntity> entities = ontology.getSignature();
//			
//			for (OWLEntity entity : entities) {
//				System.out.println(entity + ":");
//				
//					}
//				System.out.println();
//			}
			

//	public Map<String, List<String>> getRestrictions(OWLClass owlClass,OWLOntology ontology ) {
//		RestrictionVisitor visitor = new RestrictionVisitor(Collections.singleton(ontology));
//		for (OWLSubClassOfAxiom ax : ontology.getSubClassAxiomsForSubClass(owlClass)) {
//			OWLClassExpression superCls = ax.getSuperClass();
//			// Ask our superclass to accept a visit from the RestrictionVisitor
//			// - if it is an existential restriction then our restriction visitor
//			// will answer it - if not our visitor will ignore it
//			superCls.accept(visitor);
//		}
//
//		Map<String, List<String>> restrictions = new HashMap<>();
//		for (OWLObjectSomeValuesFrom val : visitor.getSomeValues()) {
//			OWLClassExpression exp = val.getFiller();
//
//			// Get the shortname of the property expression
//			String shortForm = null;
//			Set<OWLObjectProperty> signatureProps = val.getProperty().getObjectPropertiesInSignature();
//			for (OWLObjectProperty sigProp : signatureProps) {
//				Collection<String> labels = findLabels(sigProp.getIRI());
//				if (labels.size() > 0) {
//					shortForm = new ArrayList<String>(labels).get(0);
//				}
//			}
//
//			if (shortForm != null && !exp.isAnonymous()) {
//				IRI iri = exp.asOWLClass().getIRI();
//
//				if (!restrictions.containsKey(shortForm)) {
//					restrictions.put(shortForm, new ArrayList<String>());
//				}
//				restrictions.get(shortForm).add(iri.toString());
//			}
//		}
//
//		return restrictions;
//	}
//	 
}
	
	public void myVisitor(OWLDataHasValue ce)
	{
			OWLDataPropertyExpression propertyExpression = ce.getProperty();
			OWLLiteral value = ce.getFiller();
			//sparql += asTriplePattern(variables.peek(), propertyExpression.asOWLDataProperty(), value);
		
	}
}